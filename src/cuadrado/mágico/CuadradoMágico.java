/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuadrado.mágico;

import java.util.Scanner;

/**
 *
 * @author David
 */
public class CuadradoMágico {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el tamaño del cuadrado");
        int n = sc.nextInt();
        int[][] magic = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                magic[i][j] = (n * i) + j + 1;
            }
        }
        for (int i = 0; i < n / 4; i++) {
            for (int j = 0; j < n / 4; j++) {
                magic[i][j] = (n * n + 1) - magic[i][j];
            }
        }
        for (int i = 0; i < n / 4; i++) {
            for (int j = 3 * (n / 4); j < n; j++) {
                magic[i][j] = (n * n + 1) - magic[i][j];
            }
        }
        for (int i = 3 * n / 4; i < n; i++) {
            for (int j = 0; j < n / 4; j++) {
                magic[i][j] = (n * n + 1) - magic[i][j];
            }
        }
        for (int i = 3 * n / 4; i < n; i++) {
            for (int j = 3 * n / 4; j < n; j++) {
                magic[i][j] = (n * n + 1) - magic[i][j];
            }
        }
        for (int i = n / 4; i < 3 * n / 4; i++) {
            for (int j = n / 4; j < 3 * n / 4; j++) {
                magic[i][j] = (n * n + 1) - magic[i][j];
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(magic[i][j] + " | ");
            }
            System.out.println("");
        }
    }

}
